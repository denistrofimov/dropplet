//
//  DLDomain.m
//  Dropplet
//
//  Created by Denis Trofimov on 21.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import "DLDomain.h"
#import "DLAPI.h"
#import "DLDomainRecord.h"

@implementation DLDomain

+(NSString *)name{
    return @"domains";
}

-(id)uid{
    return self.name;
}

-(void)parse:(id)data{
    if(data[@"domain"])
        return [self parse:data[@"domain"]];
    [super parse:data];
}

-(void)listRecordsWithCompletion:(void (^)(NSError* error, NSArray *list))completion{
    [DLDomainRecord listWithPath:[NSString stringWithFormat:@"%@/%@/records", [[self class] name], self.uid]
                      parameters:nil
                      completion:^(NSError *error, NSArray *list) {
                          if(error)
                              return completion(error, nil);
                          DLDomainRecord *record = nil;
                          for (record in list) {
                              record.domain = self.uid;
                          }
                          completion(error, list);
                      }];
}

@end
