//
//  DLDroplet.m
//  Dropplet
//
//  Created by Denis Trofimov on 12.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import "DLDroplet.h"
#import "DLRegion.h"
#import "DLImage.h"
#import "DLSize.h"
#import "DLAPI.h"
#import "DLAction.h"

@implementation DLDroplet

+(NSString *)name{
    return @"droplets";
}

-(NSString *)ip{
    return self.networks[@"v4"][0][@"ip_address"];
}

-(id)uid{
    return self.id;
}

-(void)parse:(id)data{
    if(data[@"droplet"])
        return [self parse:data[@"droplet"]];
    
    [super parse:data];
    
    self.image = [[DLImage alloc] init];
    [self.image parse:data[@"image"]];
    
    self.size = [[DLSize alloc] init];
    [self.size parse:data[@"size"]];
    
    self.region = [[DLRegion alloc] init];
    [self.region parse:data[@"region"]];
    
}

-(void)performActionWithName:(NSString*)name parameters:(NSDictionary*)parameters completion:(void (^)(NSError *, DLAction*))completion{
    NSAssert(name, @"Action name is required");
    NSMutableDictionary *info = [NSMutableDictionary dictionary];
    [info setObject:name forKey:@"name"];
    if(parameters)
       [info addEntriesFromDictionary:parameters];
    
    [API POST:[NSString stringWithFormat:@"%@/%@/actions", [[self class] name], self.uid]
   parameters:info
      success:^(AFHTTPRequestOperation *operation, id responseObject) {
          DLAction *action = [[DLAction alloc] init];
          [action parse:responseObject];
          completion(nil, action);
      }
      failure:^(AFHTTPRequestOperation *operation, NSError *error) {
          error = (operation.responseObject[@"message"]) ? ERROR(error.code, operation.responseObject[@"message"]) : error;
          completion(error, nil);
      }];
}

-(void)listActionsWithCompletion:(void (^)(NSError* error, NSArray *list))completion{
    [API GET:[NSString stringWithFormat:@"%@/%@/actions", [[self class] name], self.uid]
   parameters:nil
      success:^(AFHTTPRequestOperation *operation, id responseObject) {
          NSMutableArray *array = [NSMutableArray array];
          for (id source in responseObject[@"actions"]) {
              id entity = [[DLAction alloc] init];
              [entity parse:source];
              [array addObject:entity];
          }
          completion(nil, array);
      }
      failure:^(AFHTTPRequestOperation *operation, NSError *error) {
          error = (operation.responseObject[@"message"]) ? ERROR(error.code, operation.responseObject[@"message"]) : error;
          completion(error, nil);
      }];
}

-(DLDropletStatus)dropletStatus{
    return (DLDropletStatus)[@[@"new",@"active",@"off",@"archive"] indexOfObject:self.status];
}

-(NSString *)localizedStatus{
    return NSLocalizedString(self.status, @"");
}

@end
