//
//  DLDroplet.h
//  Dropplet
//
//  Created by Denis Trofimov on 12.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import "DLEntity.h"

@class DLSize;
@class DLImage;
@class DLRegion;
@class DLAction;

typedef enum{
    DLDropletStatusNew,
    DLDropletStatusActive,
    DLDropletStatusOff,
    DLDropletStatusArchive
} DLDropletStatus;


@interface DLDroplet : DLEntity

/* A unique identifier for each Droplet instance. This is automatically generated upon Droplet creation. */
@property(nonatomic, strong) NSNumber *id;

/* The human-readable name set for the Droplet instance. */
@property(nonatomic, strong) NSString *name;

/* Memory of the Droplet in megabytes. */
@property(nonatomic, strong) NSNumber *memory;

/* The number of virtual CPUs. */
@property(nonatomic, strong) NSNumber *vcpus;

/* The size of the Droplet's disk in gigabytes. */
@property(nonatomic, strong) NSNumber *disk;

/* A time value given in ISO8601 combined date and time format that represents when the Droplet was created. */
@property(nonatomic, strong) NSString *created_at;

/* A status string indicating the state of the Droplet instance. This may be "new", "active", "off", or "archive". */
@property(nonatomic, strong) NSString *status;

/* The unique slug identifier for the size of this Droplet. */
@property(nonatomic, strong) NSString *size_slug;

/* A boolean value indicating whether the Droplet has been locked, preventing actions by users. */
@property(nonatomic, readwrite, getter=isLocked) BOOL locked;

/* The region that the Droplet instance is deployed in. When setting a region, the value should be the slug identifier for the region. 
 When you query a Droplet, the entire region object will be returned. */
@property(nonatomic, strong) DLRegion *region;

/* The current size object describing the Droplet. When setting a size, the value is set to the size slug. When querying the Droplet, the entire size object will be returned.
 Note that the disk volume of a droplet may not match the size's disk due to Droplet resize actions. The disk attribute on the Droplet should always be referenced. */
@property(nonatomic, strong) DLSize *size;

/* The base image used to create the Droplet instance. When setting an image, the value is set to the image id or slug. When querying the Droplet, the entire image object will be returned. */
@property(nonatomic, strong) DLImage *image;

/* An array of backup IDs of any backups that have been taken of the Droplet instance. Droplet backups are enabled at the time of the instance creation. */
@property(nonatomic, strong) NSArray *backup_ids;

/* An array of snapshot IDs of any snapshots created from the Droplet instance. */
@property(nonatomic, strong) NSArray *snapshot_ids;

/* An array of features enabled on this Droplet. */
@property(nonatomic, strong) NSArray *features;

/* The details of the network that are configured for the Droplet instance. This is an object that contains keys for IPv4 and IPv6. The value of each of these is an array that
 contains objects describing an individual IP resource allocated to the Droplet. These will define attributes like the IP address, netmask, and gateway of the specific
 network depending on the type of network it is. */
@property(nonatomic, strong) NSDictionary *networks;

/* The current kernel. This will initially be set to the kernel of the base image when the Droplet is created. */
@property(nonatomic, strong) NSDictionary *kernel;

/* The details of the Droplet's backups feature, if backups are configured for the Droplet. This object contains keys for the start and end times of the window during 
 which the backup will start. */
@property(nonatomic, strong) NSDictionary *next_backup_window;

@property(nonatomic, readonly)NSString *ip;

@property(nonatomic, readonly) DLDropletStatus dropletStatus;

@property(nonatomic, readonly) NSString *localizedStatus;

-(void)performActionWithName:(NSString*)name parameters:(NSDictionary*)parametrs completion:(void (^)(NSError *, DLAction*))completion;

-(void)listActionsWithCompletion:(void (^)(NSError* error, NSArray *list))completion;

@end
