//
//  DLSize.h
//  Dropplet
//
//  Created by Denis Trofimov on 13.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import "DLEntity.h"

//regions       array	An array containing the region slugs where this size is available for Droplet creates.

@interface DLSize : DLEntity

/* A human-readable string that is used to uniquely identify each size. */
@property(nonatomic, strong) id slug;

/* This is a boolean value that represents whether new Droplets can be created with this size. */
@property(nonatomic, readwrite, getter=isAvailable) BOOL available;

/* The amount of transfer bandwidth that is available for Droplets created in this size. This only counts traffic on the public interface. The value is given in terabytes. */
@property(nonatomic, strong) NSNumber *transfer;

/* This attribute describes the monthly cost of this Droplet size if the Droplet is kept for an entire month. The value is measured in US dollars. */
@property(nonatomic, strong) NSNumber *price_monthly;

/* This describes the price of the Droplet size as measured hourly. The value is measured in US dollars. */
@property(nonatomic, strong) NSNumber *price_hourly;

/* The amount of RAM allocated to Droplets created of this size. The value is represented in megabytes. */
@property(nonatomic, strong) NSNumber *memory;

/* The number of virtual CPUs allocated to Droplets of this size. */
@property(nonatomic, strong) NSNumber *vcpus;

/* The amount of disk space set aside for Droplets of this size. The value is represented in gigabytes. */
@property(nonatomic, strong) NSNumber *disk;

@end
