//
//  DLImage.m
//  Dropplet
//
//  Created by Denis Trofimov on 13.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import "DLImage.h"

@implementation DLImage

+(NSString *)name{
    return @"images";
}

-(id)uid{
    return self.id;
}

-(void)parse:(id)data{
    if(data[@"image"])
        return [self parse:data[@"image"]];
    [super parse:data];
}


+(void)listDistributionsWithCompletion:(void (^)(NSError* error, NSArray *list))completion{
    [super listWithParameters:@{@"type":@"distribution"} completion:completion];
}

+(void)listApplicationsWithCompletion:(void (^)(NSError* error, NSArray *list))completion{
    [super listWithParameters:@{@"type":@"application"} completion:completion];
}

+(void)listPrivatesWithCompletion:(void (^)(NSError* error, NSArray *list))completion{
    [super listWithParameters:@{@"private":@"true"} completion:completion];
}

+(void)listAllWithCompletion:(void (^)(NSError* error, NSArray *list))completion{
    NSMutableArray *all = [NSMutableArray array];
    __block NSInteger count = 3;
    
    [self listDistributionsWithCompletion:^(NSError *error, NSArray *list) {
        [all addObjectsFromArray:list];
        if(!--count)
            completion(nil, all);
    }];
    
    [self listApplicationsWithCompletion:^(NSError *error, NSArray *list) {
        [all addObjectsFromArray:list];
        if(!--count)
            completion(nil, all);
    }];
    
    [self listPrivatesWithCompletion:^(NSError *error, NSArray *list) {
        [all addObjectsFromArray:list];
        if(!--count)
            completion(nil, all);
    }];
}

-(UIImage *)distributionIcon{
    
    return [UIImage imageNamed:[self.distribution lowercaseString]];
}

@end
