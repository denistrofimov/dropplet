//
//  DLEntity.h
//  Dropplet
//
//  Created by Denis Trofimov on 13.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface DLEntity : NSObject

-(instancetype)copy;

+(NSString*)name;

+(NSString*)entry;

-(NSString*)entityURL;

+(void)listWithCompletion:(void (^)(NSError* error, NSArray *list))completion;

+(void)listWithParameters:(id)parameters completion:(void (^)(NSError* error, NSArray *list))completion;

+(void)listWithPath:(NSString*)path parameters:(id)parameters completion:(void (^)(NSError* error, NSArray *list))completion;

-(void)getWithCompletion:(void (^)(NSError* error))completion;

-(void)createWithCompletion:(void (^)(NSError* error))completion;

-(void)createWithParameters:(id)parametrs completion:(void (^)(NSError *))completion;

-(void)updateWithCompletion:(void (^)(NSError* error))completion;

-(void)saveWithCompletion:(void (^)(NSError* error))completion;

-(void)deleteWithCompletion:(void (^)(NSError* error))completion;

-(void)parse:(id)data;

-(id)toJSON;

-(id)uid;

-(void)setUid:(id)uid;

@end
