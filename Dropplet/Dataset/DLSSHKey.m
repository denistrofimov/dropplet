//
//  DLSSHKey.m
//  Dropplet
//
//  Created by Denis Trofimov on 14.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import "DLSSHKey.h"

@implementation DLSSHKey

+(NSString *)name{
    return @"account/keys";
}

+(NSString *)entry{
    return @"ssh_keys";
}

-(id)uid{
    return self.id;
}

-(void)parse:(id)data{
    if(data[@"ssh_key"])
        return [self parse:data[@"ssh_key"]];
    [super parse:data];
}

@end
