//
//  DLSize.m
//  Dropplet
//
//  Created by Denis Trofimov on 13.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import "DLSize.h"

@implementation DLSize

+(NSString *)name{
    return @"sizes";
}

-(id)uid{
    return self.slug;
}

-(void)parse:(id)data{
    if(data[@"size"])
        return [self parse:data[@"size"]];
    [super parse:data];
}

@end
