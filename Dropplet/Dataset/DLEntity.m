//
//  DLEntity.m
//  Dropplet
//
//  Created by Denis Trofimov on 13.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import "DLEntity.h"
#import "DLAPI.h"
#import <objc/runtime.h>

@implementation DLEntity


+(NSString*)name{
    return nil;
}

+(NSString*)entry{
    return [self name];
}

-(NSString*)entityURL{
    return [[self class] name];
}

+(void)listWithCompletion:(void (^)(NSError* error, NSArray *list))completion{
    [self listWithParameters:nil completion:completion];
}

+(void)listWithParameters:(id)parameters completion:(void (^)(NSError* error, NSArray *list))completion{
    [self listWithPath:[self name] parameters:parameters completion:completion];
}

+(void)listWithPath:(NSString*)path parameters:(id)parameters completion:(void (^)(NSError* error, NSArray *list))completion{
    [API getEntitiesWithName:path
                  parameters:parameters
                       entry:self.entry
                  completion:^(NSError* error, NSArray *list){
                      if(error)
                          return completion(error, nil);
                      NSMutableArray *array = [NSMutableArray array];
                      for (id source in list) {
                          id entity = [[self alloc] init];
                          [entity parse:source];
                          [array addObject:entity];
                      }
                      completion(error, array);
                  }];
}

-(void)getWithCompletion:(void (^)(NSError* error))completion{
    [API getEntityWithName:self.entityURL uid:self.uid completion:^(NSError *error, id entity) {
        [self parse:entity];
        completion(error);
    }];
}

-(void)createWithCompletion:(void (^)(NSError* error))completion{
    id data = [self toJSON];
    [API createEntityWithName:self.entityURL data:data completion:^(NSError *error, id entity) {
        if(error)
            return completion(error);
        [self parse:entity];
        completion(nil);
    }];
}

-(void)createWithParameters:(id)parametrs completion:(void (^)(NSError *))completion{
    NSDictionary *initial = [self toJSON];
    NSMutableDictionary *extended = [NSMutableDictionary dictionaryWithDictionary:initial];
    if(parametrs)
        [extended addEntriesFromDictionary:parametrs];
    
    [API createEntityWithName:[[self class] name] data:extended completion:^(NSError *error, id entity) {
        if(error)
            return completion(error);
        [self parse:entity];
        completion(nil);
    }];
}

-(void)updateWithCompletion:(void (^)(NSError* error))completion{
    id data = [self toJSON];
    [API updateEntityWithName:self.entityURL uid:self.uid data:data completion:^(NSError *error, id entity) {
        if(error)
            return completion(error);
        [self parse:entity];
        completion(nil);
    }];
}

-(void)saveWithCompletion:(void (^)(NSError* error))completion{
    if(self.uid)
        [self updateWithCompletion:completion];
    else
        [self createWithCompletion:completion];
}

-(void)deleteWithCompletion:(void (^)(NSError* error))completion{
    [API deleteEntityWithName:self.entityURL uid:self.uid completion:completion];
}

-(void)parse:(NSDictionary*)data{
    for (NSString *key in data.allKeys)
        [self setValue:data[key] forKey:key];
}

-(id)valueForUndefinedKey:(NSString *)key{
    return nil;
}

-(void)setValue:(id)value forUndefinedKey:(NSString *)key{}

-(id)toJSON{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    unsigned count;
    objc_property_t *properties = class_copyPropertyList([self class], &count);
    for (int i = 0; i < count; i++) {
        NSString *key = [NSString stringWithUTF8String:property_getName(properties[i])];
        id object = [self valueForKey:key];
        if([object respondsToSelector:@selector(uid)]){
            object = [object performSelector:@selector(uid)];
            if(!object)
                object = [NSNull null];
        }
        if([key isEqualToString:@"uid"])
            object = nil;
        
        if([key isEqualToString:@"superclass"])
            object = nil;
        
        if([key isEqualToString:@"description"])
            object = nil;
        
        if([key isEqualToString:@"debugDescription"])
            object = nil;
        
        if([object isKindOfClass:[NSURL class]])
            object = nil;
        
        if(object)
            [dict setObject:object forKey:key];
    }
    free(properties);
    return [NSDictionary dictionaryWithDictionary:dict];
}

-(NSString *)description{
    return [[self toJSON] description];
}

-(id)uid{
    return nil;
}

-(void)setUid:(id)uid{

}

-(instancetype)copy{
    id source = [self toJSON];
    DLEntity *copy = [[[self class] alloc] init];
    [copy parse:source];
    return copy;
}

@end
