//
//  DLDomainRecord.m
//  Dropplet
//
//  Created by Denis Trofimov on 21.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import "DLDomainRecord.h"

@implementation DLDomainRecord

+(NSString *)name{
    return @"domain_records";
}

-(id)uid{
    return self.id;
}

-(NSString *)entityURL{
    return [NSString stringWithFormat:@"domains/%@/records", self.domain];
}

-(void)parse:(id)data{
    if(data[@"domain_record"])
        return [self parse:data[@"domain_record"]];
    [super parse:data];
}

@end
