//
//  DLImage.h
//  Dropplet
//
//  Created by Denis Trofimov on 13.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import "DLEntity.h"
#import <UIKit/UIKit.h>

@interface DLImage : DLEntity

/* A unique number that can be used to identify and reference a specific image. */
@property(nonatomic, strong) NSNumber *id;

/* The display name that has been given to an image. This is what is shown in the control panel and is generally a descriptive title for the image in question. */
@property(nonatomic, strong) NSString *name;

/* The kind of image, describing the duration of how long the image is stored. This is one of "snapshot", "temporary" or "backup". */
@property(nonatomic, strong) NSString *type;

/* This attribute describes the base distribution used for this image. */
@property(nonatomic, strong) NSString *distribution;

/* A uniquely identifying string that is associated with each of the DigitalOcean-provided public images. These can be used to reference a public image as an alternative to the numeric id. */
@property(nonatomic, strong) NSString *slug;

/* A time value given in ISO8601 combined date and time format that represents when the Image was created. */
@property(nonatomic, strong) NSString *created_at;

/* The minimum 'disk' required for a size to use this image. */
@property(nonatomic, strong) NSNumber *min_disk_size;

/* This is a boolean value that indicates whether the image in question is public or not. An image that is public is available to all accounts. A non-public image is only accessible from your account. */
@property(nonatomic, readwrite,getter=isPublic) BOOL  public;

@property(nonatomic, readonly) UIImage *distributionIcon;

+(void)listDistributionsWithCompletion:(void (^)(NSError* error, NSArray *list))completion;

+(void)listApplicationsWithCompletion:(void (^)(NSError* error, NSArray *list))completion;

+(void)listPrivatesWithCompletion:(void (^)(NSError* error, NSArray *list))completion;

+(void)listAllWithCompletion:(void (^)(NSError* error, NSArray *list))completion;

@end
