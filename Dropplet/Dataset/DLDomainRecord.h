//
//  DLDomainRecord.h
//  Dropplet
//
//  Created by Denis Trofimov on 21.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import "DLEntity.h"

@interface DLDomainRecord : DLEntity

/* A unique identifier for each domain record */
@property(nonatomic, strong) NSNumber *id;

/* The priority for SRV and MX records */
@property(nonatomic, strong) NSNumber *priority;

/* The port for SRV records */
@property(nonatomic, strong) NSNumber *port;

/* The weight for SRV records */
@property(nonatomic, strong) NSNumber *weight;

/* The type of the DNS record (A, CNAME, TXT, ...) */
@property(nonatomic, strong) NSString *type;

/* The name to use for the DNS record */
@property(nonatomic, strong) NSString *name;

/* The value to use for the DNS record */
@property(nonatomic, strong) NSString *data;

/* Them domain name */
@property(nonatomic, strong) NSString *domain;

@end
