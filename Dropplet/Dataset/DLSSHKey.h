//
//  DLSSHKey.h
//  Dropplet
//
//  Created by Denis Trofimov on 14.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import "DLEntity.h"

@interface DLSSHKey : DLEntity

/* This is a unique identification number for the key. This can be used to reference a specific SSH key when you wish to embed a key into a Droplet. */
@property(nonatomic, strong) NSNumber *id;

/* This is the human-readable display name for the given SSH key. This is used to easily identify the SSH keys when they are displayed. */
@property(nonatomic, strong) NSString *name;

/* This attribute contains the fingerprint value that is generated from the public key. This is a unique identifier that will differentiate it from other keys using a format that SSH recognizes. */
@property(nonatomic, strong) NSString *fingerprint;

/* This attribute contains the entire public key string that was uploaded. This is what is embedded into the 
 root user's authorized_keys file if you choose to include this SSH key during Droplet creation. */
@property(nonatomic, strong) NSString *public_key;

@end
