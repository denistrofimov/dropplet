//
//  DLAction.m
//  Dropplet
//
//  Created by Denis Trofimov on 18.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import "DLAction.h"

@implementation DLAction

+(NSString *)name{
    return @"actions";
}

+(NSArray*)types{
    return @[
             @"reboot",
             @"power_cycle",
             @"shutdown",
             @"power_off",
             @"power_on",
             @"password_reset",
             @"enable_ipv6",
             @"enable_private_networking",
             @"snapshot",
             @"upgrade",
             
             // need image - list images to select
             @"restore",
             
             // need image - list images to select
             @"rebuild",
             
             // need size, disk - list sizes to select
             @"resize",
             
             // need name - input for name
             @"rename",
             
             // need kernel ?
             //@"change_kernel"
             ];
}

+(BOOL)isSimple:(NSString*)type{
    return [@[
             @"reboot",
             @"power_cycle",
             @"shutdown",
             @"power_off",
             @"power_on",
             @"password_reset",
             @"enable_ipv6",
             @"enable_private_networking",
             @"snapshot",
             @"upgrade"] containsObject:type];

}

-(id)uid{
    return self.id;
}

-(void)parse:(id)data{
    if(data[@"action"])
        return [self parse:data[@"action"]];
    [super parse:data];
}

-(DLActionStatus)actionStatus{
    return (DLActionStatus)[@[@"in-progress", @"completed",  @"errored"] indexOfObject:self.status];
}

-(NSString *)localizedStatus{
    return NSLocalizedString(self.status, @"Status");
}

-(NSString *)localizedType{
    return NSLocalizedString(self.type, @"Type");
}

@end
