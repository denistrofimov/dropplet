//
//  DLAction.h
//  Dropplet
//
//  Created by Denis Trofimov on 18.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import "DLEntity.h"

typedef enum{
    DLActionStatusInProgress,
    DLActionStatusCompleted,
    DLActionStatusErrored
} DLActionStatus;

@interface DLAction : DLEntity

+(NSArray*)types;

+(BOOL)isSimple:(NSString*)type;

/* A unique numeric ID that can be used to identify and reference an action */
@property(nonatomic, strong) NSNumber * id;

/* The current status of the action. This can be "in-progress", "completed", or "errored" */
@property(nonatomic, strong) NSString * status;

/* This is the type of action that the object represents. For example, this could be "transfer" to represent the state of an image transfer action */
@property(nonatomic, strong) NSString * type;

/* A time value given in ISO8601 combined date and time format that represents when the action was initiated */
@property(nonatomic, strong) NSString * started_at;

/* A time value given in ISO8601 combined date and time format that represents when the action was completed */
@property(nonatomic, strong) NSString * completed_at;

/* The type of resource that the action is associated with */
@property(nonatomic, strong) NSString * resource_type;

/* A unique identifier for the resource that the action is associated with */
@property(nonatomic, strong) NSNumber * resource_id;

@property(nonatomic, readonly) DLActionStatus actionStatus;

@property(nonatomic, readonly) NSString *localizedStatus;

@property(nonatomic, readonly) NSString *localizedType;

@end
