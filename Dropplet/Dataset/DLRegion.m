//
//  DLRegion.m
//  Dropplet
//
//  Created by Denis Trofimov on 13.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import "DLRegion.h"

@implementation DLRegion

+(NSString *)name{
    return @"regions";
}

-(id)uid{
    return self.slug;
}

-(void)parse:(id)data{
    if(data[@"region"])
        return [self parse:data[@"region"]];
    [super parse:data];
}

-(UIImage *)flag{
    NSString *imageName = @{
                            //netherlands
                            @"ams1":@"netherlands",
                            @"ams2":@"netherlands",
                            @"ams3":@"netherlands",
                            
                            // usa
                            @"nyc1":@"usa",
                            @"nyc2":@"usa",
                            @"nyc3":@"usa",
                            @"sfo1":@"usa",
                            @"sfo2":@"usa",
                            @"sfo3":@"usa",
                            
                            // singapore
                            @"sgp1":@"singapore",
                            @"sgp2":@"singapore",
                            @"sgp3":@"singapore",
                            
                            // uk
                            @"lon1":@"uk",
                            @"lon2":@"uk",
                            @"lon3":@"uk",
                            
                            // germany
                            @"fra1":@"germany",
                            @"fra2":@"germany",
                            @"fra3":@"germany",
                            }[self.slug];
    return [UIImage imageNamed:imageName];
}

@end
