//
//  DLRegion.h
//  Dropplet
//
//  Created by Denis Trofimov on 13.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import "DLEntity.h"

//sizes	array	This attribute is set to an array which contains the identifying slugs for the sizes available in this region.
//features	array	This attribute is set to an array which contains features available in this region

@interface DLRegion : DLEntity

/* A human-readable string that is used as a unique identifier for each region. */
@property(nonatomic, strong) NSString *slug;

/* The display name of the region. This will be a full name that is used in the control panel and other interfaces. */
@property(nonatomic, strong) NSString *name;

/* This is a boolean value that represents whether new Droplets can be created in this region. */
@property(nonatomic, readwrite, getter=isAvailable) BOOL available;

@property(nonatomic, readonly) UIImage *flag;

@end
