//
//  DLTouchID.m
//  Dropplet
//
//  Created by Denis Trofimov on 20.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import "DLTouchID.h"
#import <LocalAuthentication/LocalAuthentication.h>

@implementation DLTouchID

+(void)authenticateUserWithReason:(NSString *)reason
                       completion:(void (^)(NSError*error))completion;
{
    LAContext *context = [[LAContext alloc] init];
    NSError *error = nil;
    if ([context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&error])
        return [context evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics
                localizedReason:reason
                          reply:^(BOOL success, NSError *error) {
                              dispatch_async(dispatch_get_main_queue(), ^{
                                  completion(error);
                              });
                          }];
    completion(error);
}

@end
