//
//  DLSwipeToDeleteTableViewCell.m
//  Dropplet
//
//  Created by Denis Trofimov on 22.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import "DLSwipeToDeleteTableViewCell.h"
#import "NSMutableArray+SWUtilityButtons.h"
#import "UIColor+RGB.h"

@implementation DLSwipeToDeleteTableViewCell

-(void)awakeFromNib{
    self.rightUtilityButtons = [self buttons];
}

- (NSArray *)buttons
{
    NSMutableArray *buttons = [NSMutableArray new];
    [buttons sw_addUtilityButtonWithColor:[UIColor colorWithRGB:0xED4F32] icon:[UIImage imageNamed:@"icon-delete"]];
    return buttons;
}

- (void)hideUtilityButtonsAnimated:(BOOL)animated{
    id delegate = self.delegate;
    self.delegate = nil;
    [super hideUtilityButtonsAnimated:animated];
    self.delegate = delegate;
}

@end
