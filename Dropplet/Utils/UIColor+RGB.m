//
//  UIViewController+UIColor.m
//  Dropplet
//
//  Created by Denis Trofimov on 21.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import "UIColor+RGB.h"

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@implementation UIColor (RGB)

+(instancetype)colorWithRGB:(NSInteger)rgbValue{
    return UIColorFromRGB(rgbValue);
}

@end
