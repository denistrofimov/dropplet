//
//  DLAlertController.m
//  Dropplet
//
//  Created by Denis Trofimov on 23.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import "DLAlertController.h"

@implementation DLAlertController

+(instancetype)confirmWithTitle:(NSString*)title message:(NSString*)message action:(NSString*)action handler:(void (^)())handler{
    DLAlertController* alert = [DLAlertController alertWithTitle:title message:message];
    [alert addAction:[PSTAlertAction actionWithTitle:action handler:^(PSTAlertAction *action) {
        if(handler)
            handler();
    }]];
    [alert addCancelActionWithHandler:nil];
    [alert showWithSender:nil controller:nil animated:YES completion:nil];
    return alert;
}

+(instancetype)alertWithTitle:(NSString*)title message:(NSString*)message action:(NSString*)action handler:(void (^)())handler{
    DLAlertController* alert = [DLAlertController alertWithTitle:title message:message];
    [alert addAction:[PSTAlertAction actionWithTitle:action handler:^(PSTAlertAction *action) {
        if(handler)
            handler();
    }]];
    return alert;
}

+(instancetype)confirmWithTitle:(NSString*)title message:(NSString*)message handler:(void (^)())handler{
    return [self confirmWithTitle:title message:message action:@"Yes" handler:handler];
}

+(instancetype)alertWithTitle:(NSString*)title message:(NSString*)message handler:(void (^)())handler{
    return [self alertWithTitle:title message:message action:@"OK" handler:handler];
}

@end
