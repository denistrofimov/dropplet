//
//  UIViewController+UIColor.h
//  Dropplet
//
//  Created by Denis Trofimov on 21.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (RGB)

+(instancetype)colorWithRGB:(NSInteger)rgbValue;

@end
