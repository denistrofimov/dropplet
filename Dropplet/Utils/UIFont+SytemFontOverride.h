//
//  UIFont+SytemFontOverride.h
//  Dropplet
//
//  Created by Denis Trofimov on 22.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (SytemFontOverride)

@end
