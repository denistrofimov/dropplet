//
//  DLAlertController.h
//  Dropplet
//
//  Created by Denis Trofimov on 23.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import "PSTAlertController.h"

#define ALERT(_title_, _description_) [[DLAlertController alertWithTitle:(_title_) message:(_description_) handler:nil] showWithSender:nil controller:nil animated:YES completion:nil]

@interface DLAlertController : PSTAlertController

+(instancetype)confirmWithTitle:(NSString*)title message:(NSString*)message handler:(void (^)())handler;

+(instancetype)confirmWithTitle:(NSString*)title message:(NSString*)message action:(NSString*)action handler:(void (^)())handler;

+(instancetype)alertWithTitle:(NSString*)title message:(NSString*)message handler:(void (^)())handler;

+(instancetype)alertWithTitle:(NSString*)title message:(NSString*)message action:(NSString*)action handler:(void (^)())handler;

@end
