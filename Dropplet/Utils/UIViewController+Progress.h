//
//  UIViewController+Progress.h
//  Dropplet
//
//  Created by Denis Trofimov on 25.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^Done)();
typedef void(^Progress)(float progress);

@interface UIViewController (Progress)

-(void)execute:(void (^)(Progress progress, Done done))execute;

@end
