//
//  DLTouchID.h
//  Dropplet
//
//  Created by Denis Trofimov on 20.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface DLTouchID : NSObject

+(void)authenticateUserWithReason:(NSString *)reason
                       completion:(void (^)(NSError*error))completion;

@end
