//
//  UIFont+SytemFontOverride.m
//  Dropplet
//
//  Created by Denis Trofimov on 22.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import "UIFont+SytemFontOverride.h"
#import <objc/runtime.h>

@implementation UIFont (SytemFontOverride)

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wobjc-protocol-method-implementation"

+(void)load {
    SEL original = @selector(systemFontOfSize:);
    SEL modified = @selector(regularFontWithSize:);
    SEL originalBold = @selector(boldSystemFontOfSize:);
    SEL modifiedBold = @selector(boldFontWithSize:);
    
    Method originalMethod = class_getClassMethod(self, original);
    Method modifiedMethod = class_getClassMethod(self, modified);
    method_exchangeImplementations(originalMethod, modifiedMethod);
    
    Method originalBoldMethod = class_getClassMethod(self, originalBold);
    Method modifiedBoldMethod = class_getClassMethod(self, modifiedBold);
    method_exchangeImplementations(originalBoldMethod, modifiedBoldMethod);
}

+ (UIFont *)boldFontWithSize:(CGFloat)fontSize {
    return [UIFont fontWithName:@"BPreplay" size:fontSize];
}

+ (UIFont *)regularFontWithSize:(CGFloat)fontSize {
    return [UIFont fontWithName:@"BPreplay" size:fontSize];
}

#pragma clang diagnostic pop

@end
