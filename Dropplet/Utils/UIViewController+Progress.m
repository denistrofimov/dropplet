//
//  UIViewController+Progress.m
//  Dropplet
//
//  Created by Denis Trofimov on 25.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import "UIViewController+Progress.h"
#import "MBProgressHUD.h"

@implementation UIViewController (Progress)

-(void)execute:(void (^)(Progress progress, Done done))execute{
    NSAssert(execute != nil, @"Execute block can not be nil");
    MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    Done done = ^{
        [progressHUD hide:YES];
    };
    Progress progress = ^(float progress) {
        [progressHUD setProgress:progress];
    };
    execute(progress,done);
}

@end
