//
//  DLDropletTableViewCell.h
//  Dropplet
//
//  Created by Denis Trofimov on 27.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import "DLSwipeToDeleteTableViewCell.h"

@class DLDroplet;

@interface DLDropletTableViewCell : DLSwipeToDeleteTableViewCell

@property(nonatomic, strong)DLDroplet *droplet;

@end
