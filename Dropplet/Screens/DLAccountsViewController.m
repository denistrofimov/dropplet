//
//  DLAccountsViewController.m
//  Dropplet
//
//  Created by Denis Trofimov on 19.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import "DLAccountsViewController.h"
#import "DLAPI.h"
#import "DLSwipeToDeleteTableViewCell.h"
#import "UIViewController+ECSlidingViewController.h"
#import "DLAlertController.h"

@interface DLAccountsViewController ()<SWTableViewCellDelegate>

@property(nonatomic, strong)NSArray *accounts;
@property(nonatomic, strong)NSIndexPath *deleteIndexPath;

@end

@implementation DLAccountsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.accounts = API.accounts;
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.accounts.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SWTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"account" forIndexPath:indexPath];
    NSDictionary *account = self.accounts[indexPath.row];
    cell.textLabel.text = account[@"acct"];
    cell.delegate = self;
    if([API.account isEqualToString:account[@"acct"]])
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    else
        cell.accessoryType = UITableViewCellAccessoryNone;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *account = self.accounts[indexPath.row];
    API.account = account[@"acct"];
    [self.tableView reloadData];
}


-(BOOL)swipeableTableViewCell:(SWTableViewCell *)cell canSwipeToState:(SWCellState)state{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    return self.slidingViewController.currentTopViewPosition == ECSlidingViewControllerTopViewPositionCentered && ![self.accounts[indexPath.row][@"account"] isEqualToString:API.account];
}

- (void)swipeableTableViewCell:(SWTableViewCell *)cell scrollingToState:(SWCellState)state{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    switch(state){
        case kCellStateCenter:
            self.slidingViewController.panGesture.enabled = YES;
            self.deleteIndexPath = nil;
            break;
        case kCellStateLeft:
        case kCellStateRight:{
            self.slidingViewController.panGesture.enabled = NO;
            if(self.deleteIndexPath && self.deleteIndexPath.row != indexPath.row)
                [(DLSwipeToDeleteTableViewCell*)[self.tableView cellForRowAtIndexPath:self.deleteIndexPath] hideUtilityButtonsAnimated:YES];
            self.deleteIndexPath = indexPath;
        }
            break;
    }
}

- (void)swipeableTableViewCell:(DLSwipeToDeleteTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index {
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    [DLAlertController confirmWithTitle:NSLocalizedString(@"Warning", nil)
                                message:[NSString stringWithFormat:NSLocalizedString(@"delete entity warning", nil), NSLocalizedString(@"account", nil)]
                                 action:NSLocalizedString(@"Delete", nil)
                                handler:^{
        NSDictionary *account = self.accounts[indexPath.row];
        [API deleteAccount:account[@"acct"]];
        self.accounts = API.accounts;
        [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }];
}

@end
