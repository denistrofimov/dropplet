//
//  DLDomainViewController.h
//  Dropplet
//
//  Created by Denis Trofimov on 24.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DLDomain;

@interface DLDomainViewController : UITableViewController

@property(nonatomic, strong)DLDomain *domain;

@end
