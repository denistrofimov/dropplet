//
//  DLDomainRecordViewController.m
//  Dropplet
//
//  Created by Denis Trofimov on 24.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import "DLDomainRecordViewController.h"
#import "DLDomainRecord.h"
#import "DLAlertController.h"
#import "UIViewController+Progress.h"

@implementation DLDomainRecordViewController

-(void)viewDidLoad{
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon-ok"] style:UIBarButtonItemStylePlain target:self action:@selector(doneAction:)];
}

-(void)doneAction:(id)sender{
    [self execute:^(Progress progress, Done done) {
        [self.record saveWithCompletion:^(NSError *error) {
            done();
            if(error)
                return ALERT(@"error", error.localizedDescription);
            [self.navigationController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
        }];
    }];
}

@end
