//
//  DLNSDomainRecordViewController.m
//  Dropplet
//
//  Created by Denis Trofimov on 24.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import "DLNSDomainRecordViewController.h"
#import "DLDomainRecord.h"
#import "DLAlertController.h"

@interface DLNSDomainRecordViewController ()

@property (weak, nonatomic) IBOutlet UITextField *hostTextField;

@end

@implementation DLNSDomainRecordViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    self.hostTextField.text = self.record.data;
}

-(void)doneAction:(id)sender{
    if(!self.hostTextField.text.length)
        return ALERT(@"error",@"empty host field error");
    self.record.data = self.hostTextField.text;
    if(![self.record.data hasSuffix:@"."])
        self.record.data = [NSString stringWithFormat:@"%@.", self.record.data];
    [super doneAction:sender];
}

@end
