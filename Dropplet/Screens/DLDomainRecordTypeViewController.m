//
//  DLDomainRecordTypeViewController.m
//  Dropplet
//
//  Created by Denis Trofimov on 24.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import "DLDomainRecordTypeViewController.h"
#import "DLDomainRecord.h"
#import "DLDomainRecordViewController.h"

@interface DLDomainRecordTypeViewController ()

@property(nonatomic, strong)NSArray *types;

- (IBAction)cancelAction:(id)sender;
- (IBAction)nextAction:(id)sender;

@end

@implementation DLDomainRecordTypeViewController

-(void)viewDidLoad{
    self.types = @[@"a",
                   @"aaaa",
                   @"cname",
                   @"mx",
                   @"txt",
                   @"srv",
                   @"ns"];
}

- (IBAction)cancelAction:(id)sender {
    [self.navigationController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)nextAction:(id)sender {
    [self performSegueWithIdentifier:self.record.type sender:sender];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    DLDomainRecordViewController *controller = [segue destinationViewController];
    controller.record = self.record;
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.types.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"record_type" forIndexPath:indexPath];
    NSString *type = [self.types[indexPath.row] uppercaseString];
    cell.textLabel.text = type;
    if([self.record.type isEqualToString:type])
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    else
        cell.accessoryType = UITableViewCellAccessoryNone;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *type = [self.types[indexPath.row] uppercaseString];
    self.record.type = type;
    [self.tableView reloadData];
}

@end
