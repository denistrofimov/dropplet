//
//  ADAddDropletViewController.m
//  Dropplet
//
//  Created by Denis Trofimov on 13.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import "DLDropletSizeViewController.h"
#import "DLDroplet.h"
#import "DLSize.h"
#import "UIViewController+Progress.h"
#import "DLDropletImageViewController.h"

@interface DLDropletSizeViewController ()

@property (weak, nonatomic) IBOutlet UIBarButtonItem *cancelButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *nextButton;
@property (nonatomic, strong) NSArray* sizes;
@property (nonatomic, strong) NSIndexPath *selectedPath;

- (IBAction)cancelAction:(id)sender;

@end

@implementation DLDropletSizeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.droplet = [[DLDroplet alloc] init];
    [self execute:^(Progress progress, Done done) {
        [DLSize listWithCompletion:^(NSError *error, NSArray *list) {
            done();
            self.sizes = list;
            [self.tableView reloadData];
        }];
    }];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    DLDropletImageViewController* controller = [segue destinationViewController];
    controller.droplet = self.droplet;
}

- (IBAction)cancelAction:(id)sender {
    [self.navigationController.presentingViewController dismissViewControllerAnimated:YES
                                                                           completion:nil];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.sizes.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"size"];
    DLSize *size = self.sizes[indexPath.row];
    cell.textLabel.text = [NSString stringWithFormat:@"%@m / %@xCPU / %@GB / %@TB", size.memory, size.vcpus, size.disk, size.transfer];
    
    if([size.uid isEqualToString:self.droplet.size.uid])
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    else
        cell.accessoryType = UITableViewCellAccessoryNone;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(self.selectedPath)
        [tableView cellForRowAtIndexPath:self.selectedPath].accessoryType = UITableViewCellAccessoryNone;
    [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
    self.selectedPath = indexPath;
    self.droplet.size = self.sizes[indexPath.row];
    self.nextButton.enabled = YES;
}



@end
