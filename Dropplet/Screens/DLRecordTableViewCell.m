//
//  DLRecordTableViewCell.m
//  Dropplet
//
//  Created by Denis Trofimov on 25.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import "DLRecordTableViewCell.h"
#import "DLDomainRecord.h"

@interface DLRecordTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *typeLabel;
@property (weak, nonatomic) IBOutlet UILabel *dataLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@end

@implementation DLRecordTableViewCell

-(void)awakeFromNib{
    [super awakeFromNib];
    self.typeLabel.layer.cornerRadius = 4;
    self.typeLabel.layer.masksToBounds = YES;
}

-(void)setRecord:(DLDomainRecord *)record{
    _record = record;
    self.dataLabel.text = _record.data;
    self.nameLabel.text = _record.name;
    self.typeLabel.text = _record.type;
}

@end
