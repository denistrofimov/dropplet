//
//  DLRecordTableViewCell.h
//  Dropplet
//
//  Created by Denis Trofimov on 25.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import "DLSwipeToDeleteTableViewCell.h"


@class DLDomainRecord;

@interface DLRecordTableViewCell : DLSwipeToDeleteTableViewCell

@property(nonatomic, strong)DLDomainRecord *record;

@end
