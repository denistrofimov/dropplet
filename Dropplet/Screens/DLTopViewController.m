//
//  DLTopViewController.m
//  Dropplet
//
//  Created by Denis Trofimov on 19.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import "DLTopViewController.h"
#import "UIViewController+ECSlidingViewController.h"

@interface DLTopViewController ()<UIGestureRecognizerDelegate>

@end

@implementation DLTopViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view addGestureRecognizer:self.slidingViewController.panGesture];
    self.slidingViewController.panGesture.delegate = self;
    self.view.layer.shadowOpacity = 0.75f;
    self.view.layer.shadowRadius  = 10.0f;
    self.view.layer.shadowColor   = [UIColor blackColor].CGColor;
    self.view.layer.shadowPath    = [UIBezierPath bezierPathWithRect:self.view.bounds].CGPath;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    return YES;
}

@end
