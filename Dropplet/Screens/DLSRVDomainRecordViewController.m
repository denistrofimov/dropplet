//
//  DLSRVDomainRecordViewController.m
//  Dropplet
//
//  Created by Denis Trofimov on 24.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import "DLSRVDomainRecordViewController.h"
#import "DLDomainRecord.h"
#import "DLAlertController.h"

@interface DLSRVDomainRecordViewController()

@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *hostnameTextField;
@property (weak, nonatomic) IBOutlet UITextField *priorityTextField;
@property (weak, nonatomic) IBOutlet UITextField *portTextField;
@property (weak, nonatomic) IBOutlet UITextField *weightTextField;

@end

@implementation DLSRVDomainRecordViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    self.nameTextField.text = self.record.name;
    self.hostnameTextField.text = self.record.data;
    self.priorityTextField.text = [self.record.priority stringValue];
    self.portTextField.text = [self.record.port stringValue];
    self.weightTextField.text = [self.record.weight stringValue];
}

-(void)doneAction:(id)sender{
    if(!self.nameTextField.text.length)
        return ALERT(@"error",@"empty name field error");
    
    if(!self.hostnameTextField.text.length)
        return ALERT(@"error",@"empty hostname field error");
    
    if(!self.priorityTextField.text.length)
        return ALERT(@"error",@"empty priority field error");
    
    if(!self.portTextField.text.length)
        return ALERT(@"error",@"empty port field error");
    
    if(!self.weightTextField.text.length)
        return ALERT(@"error",@"empty weight field error");
    
    self.record.data = self.hostnameTextField.text;
    self.record.name = self.nameTextField.text;
    self.record.priority = @([self.priorityTextField.text integerValue]);
    self.record.port = @([self.portTextField.text integerValue]);
    self.record.weight = @([self.weightTextField.text integerValue]);
    
    [super doneAction:sender];
}

@end
