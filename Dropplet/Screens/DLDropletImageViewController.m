//
//  DLDropletImageViewController.m
//  Dropplet
//
//  Created by Denis Trofimov on 18.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import "DLDropletImageViewController.h"
#import "DLSSHKeysViewController.h"
#import "UIViewController+Progress.h"
#import "DLDroplet.h"
#import "DLImage.h"

@interface DLDropletImageViewController ()

@property (weak, nonatomic) IBOutlet UIBarButtonItem *nextButton;
@property (nonatomic, strong)NSArray *images;
@property (nonatomic, strong) NSIndexPath *selectedPath;

@end

@implementation DLDropletImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self execute:^(Progress progress, Done done) {
        [DLImage listAllWithCompletion:^(NSError *error, NSArray *list) {
            done();
            self.images = list;
            [self.tableView reloadData];
        }];
    }];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    DLSSHKeysViewController* controller = [segue destinationViewController];
    controller.droplet = self.droplet;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.images.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"image"];
    DLImage *image = self.images[indexPath.row];
    cell.textLabel.text = image.name;
    cell.detailTextLabel.text = image.distribution;
    cell.imageView.image = image.distributionIcon;
    if(self.droplet.image && [image.uid isEqualToNumber:self.droplet.image.uid])
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    else
        cell.accessoryType = UITableViewCellAccessoryNone;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(self.selectedPath)
        [tableView cellForRowAtIndexPath:self.selectedPath].accessoryType = UITableViewCellAccessoryNone;
    [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
    self.selectedPath = indexPath;
    self.droplet.image = self.images[indexPath.row];
    self.nextButton.enabled = YES;
}

@end
