//
//  DLADomainRecordViewController.m
//  Dropplet
//
//  Created by Denis Trofimov on 24.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import "DLADomainRecordViewController.h"
#import "DLDomainRecord.h"
#import "DLAlertController.h"

@interface DLADomainRecordViewController()

@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *ipTextField;

@end

@implementation DLADomainRecordViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    self.nameTextField.text = self.record.name;
    self.ipTextField.text = self.record.data;
}

-(void)doneAction:(id)sender{
    if(!self.nameTextField.text.length)
        return ALERT(@"error",@"empty name field error");
    
    if(!self.ipTextField.text.length)
        return ALERT(@"error",@"empty ip address field error");
    
    self.record.data = self.ipTextField.text;
    self.record.name = self.nameTextField.text;
    
    [super doneAction:sender];
}

@end
