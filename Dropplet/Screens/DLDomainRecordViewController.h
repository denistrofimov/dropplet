//
//  DLDomainRecordViewController.h
//  Dropplet
//
//  Created by Denis Trofimov on 24.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DLDomainRecord;

@interface DLDomainRecordViewController : UIViewController

@property(nonatomic, strong) DLDomainRecord *record;

-(void)doneAction:(id)sender;

@end
