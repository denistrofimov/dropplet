//
//  DLSSHKeysViewController.m
//  Dropplet
//
//  Created by Denis Trofimov on 18.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import "DLSSHKeysViewController.h"
#import "DLCreateDropletViewController.h"
#import "UIViewController+Progress.h"
#import "DLSSHKey.h"

@interface DLSSHKeysViewController ()

@property (weak, nonatomic) IBOutlet UIBarButtonItem *nextButton;
@property (nonatomic, strong)NSArray *keys;
@property (nonatomic, strong)NSMutableSet *selectedKeys;

@end

@implementation DLSSHKeysViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self execute:^(Progress progress, Done done) {
        [DLSSHKey listWithCompletion:^(NSError *error, NSArray *list) {
            done();
            self.keys = list;
            [self.tableView reloadData];
        }];
    }];
}

-(void)next:(id)sender{
    [self performSegueWithIdentifier:@"info" sender:sender];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.keys.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"key" forIndexPath:indexPath];
    DLSSHKey *key = self.keys[indexPath.row];
    cell.textLabel.text = key.name;
    
    if([self.selectedKeys containsObject:key])
        [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
    else
        [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    DLSSHKey *key = self.keys[indexPath.row];
    if([self.selectedKeys containsObject:key]){
        [self.selectedKeys removeObject:key];
        [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
    }else{
        [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
        [self.selectedKeys addObject:key];
    }
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    DLCreateDropletViewController* controller = [segue destinationViewController];
    controller.droplet = self.droplet;
    controller.keys = self.selectedKeys.allObjects;
}

@end
