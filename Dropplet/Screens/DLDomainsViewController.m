//
//  DLDomainsViewController.m
//  Dropplet
//
//  Created by Denis Trofimov on 21.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import "DLDomainsViewController.h"
#import "DLDomain.h"
#import "DLSwipeToDeleteTableViewCell.h"
#import "UIViewController+ECSlidingViewController.h"
#import "DLAlertController.h"
#import "DLDomainViewController.h"
#import "UIViewController+Progress.h"

@interface DLDomainsViewController ()<SWTableViewCellDelegate>

@property(nonatomic, strong)NSArray *domains;
@property(nonatomic, strong)NSIndexPath *deleteIndexPath;

@end

@implementation DLDomainsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated{
    [self.tableView reloadData];
    [self execute:^(Progress progress, Done done) {
        [DLDomain listWithCompletion:^(NSError *error, NSArray *list) {
            done();
            self.domains = list;
            [self.tableView reloadSections:[[NSIndexSet alloc] initWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
        }];
    }];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"domain"]){
        DLDomainViewController *controller = [segue destinationViewController];
        controller.domain = sender;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self performSegueWithIdentifier:@"domain" sender:self.domains[indexPath.row]];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.domains.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SWTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"domain"];
    DLDomain *domain = self.domains[indexPath.row];
    cell.delegate = self;
    cell.textLabel.text = domain.name;
    return cell;
}

-(BOOL)swipeableTableViewCell:(SWTableViewCell *)cell canSwipeToState:(SWCellState)state{
    return self.slidingViewController.currentTopViewPosition == ECSlidingViewControllerTopViewPositionCentered;
}

- (void)swipeableTableViewCell:(SWTableViewCell *)cell scrollingToState:(SWCellState)state{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    switch(state){
        case kCellStateCenter:
            self.slidingViewController.panGesture.enabled = YES;
            self.deleteIndexPath = nil;
            break;
        case kCellStateLeft:
        case kCellStateRight:{
            self.slidingViewController.panGesture.enabled = NO;
            if(self.deleteIndexPath && self.deleteIndexPath.row != indexPath.row)
                [(DLSwipeToDeleteTableViewCell*)[self.tableView cellForRowAtIndexPath:self.deleteIndexPath] hideUtilityButtonsAnimated:YES];
            self.deleteIndexPath = indexPath;
        }
            break;
    }
}

- (void)swipeableTableViewCell:(DLSwipeToDeleteTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index {
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    [DLAlertController confirmWithTitle:NSLocalizedString(@"Warning", nil)
                                message:[NSString stringWithFormat:NSLocalizedString(@"delete entity warning", nil), NSLocalizedString(@"domain", nil)]
                                 action:NSLocalizedString(@"Delete",nil)
                                handler:^{
                                    DLDomain *domain = self.domains[indexPath.row];
                                    [self execute:^(Progress progress, Done done) {
                                        [domain deleteWithCompletion:^(NSError *error) {
                                            done();
                                            if(error)
                                                return ALERT(@"error",error.localizedDescription);
                                            NSMutableArray *copy = self.domains.mutableCopy;
                                            [copy removeObject:domain];
                                            self.domains = [NSArray arrayWithArray:copy];
                                            [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                                        }];
                                    }];
                                }];
}


@end
