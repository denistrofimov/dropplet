//
//  DLCreateDropletViewController.h
//  Dropplet
//
//  Created by Denis Trofimov on 18.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DLDroplet;

@interface DLCreateDropletViewController : UITableViewController

@property(nonatomic, strong)NSArray *keys;
@property(nonatomic, strong) DLDroplet *droplet;

@end
