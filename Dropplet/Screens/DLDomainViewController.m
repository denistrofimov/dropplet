//
//  DLDomainViewController.m
//  Dropplet
//
//  Created by Denis Trofimov on 24.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import "DLDomainViewController.h"
#import "DLDomain.h"
#import "DLRecordTableViewCell.h"
#import "UIViewController+ECSlidingViewController.h"
#import "DLAlertController.h"
#import "DLDomainRecord.h"
#import "DLDomainRecordTypeViewController.h"
#import "UIViewController+Progress.h"

@interface DLDomainViewController ()<SWTableViewCellDelegate>

@property(nonatomic, strong)NSArray *records;
@property(nonatomic, strong)NSIndexPath *deleteIndexPath;

@end

@implementation DLDomainViewController

-(void)viewWillAppear:(BOOL)animated{
    self.title = self.domain.name;
    
    [self execute:^(Progress progress, Done done) {
        [self.domain listRecordsWithCompletion:^(NSError *error, NSArray *list) {
            done();
            self.records = list;
            [self.tableView reloadSections:[[NSIndexSet alloc] initWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
        }];
    }];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"record"]){
        UINavigationController * navigationController = [segue destinationViewController];
        DLDomainRecordTypeViewController *controller = (DLDomainRecordTypeViewController*)navigationController.topViewController;
        if([sender isKindOfClass:[DLDomainRecord class]]){
            DLDomainRecord *source = sender;
            controller.record = [source copy];
        }else{
            DLDomainRecord* record = [[DLDomainRecord alloc] init];
            record.type = @"A";
            record.domain = self.domain.uid;
            controller.record = record;
        }
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.records.count;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self performSegueWithIdentifier:@"record" sender:self.records[indexPath.row]];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    DLRecordTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"record"];
    DLDomainRecord *record = self.records[indexPath.row];
    cell.delegate = self;
    cell.record = record;
    return cell;
}

-(BOOL)swipeableTableViewCell:(SWTableViewCell *)cell canSwipeToState:(SWCellState)state{
    return self.slidingViewController.currentTopViewPosition == ECSlidingViewControllerTopViewPositionCentered;
}

- (void)swipeableTableViewCell:(SWTableViewCell *)cell scrollingToState:(SWCellState)state{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    switch(state){
        case kCellStateCenter:
            self.slidingViewController.panGesture.enabled = YES;
            self.deleteIndexPath = nil;
            break;
        case kCellStateLeft:
        case kCellStateRight:{
            self.slidingViewController.panGesture.enabled = NO;
            if(self.deleteIndexPath && self.deleteIndexPath.row != indexPath.row)
                [(DLSwipeToDeleteTableViewCell*)[self.tableView cellForRowAtIndexPath:self.deleteIndexPath] hideUtilityButtonsAnimated:YES];
            self.deleteIndexPath = indexPath;
        }
            break;
    }
}

- (void)swipeableTableViewCell:(DLSwipeToDeleteTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index {
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    [DLAlertController confirmWithTitle:NSLocalizedString(@"Warning", nil)
                                message:[NSString stringWithFormat:NSLocalizedString(@"delete entity warning", nil), NSLocalizedString(@"record", nil)]
                                 action:NSLocalizedString(@"Delete",nil)
                                handler:^{
                                    DLDomainRecord *record = self.records[indexPath.row];
                                    [self execute:^(Progress progress, Done done) {
                                        [record deleteWithCompletion:^(NSError *error) {
                                            done();
                                            if(error)
                                                return ALERT(@"error",error.localizedDescription);
                                            NSMutableArray *copy = self.records.mutableCopy;
                                            [copy removeObject:record];
                                            self.records = [NSArray arrayWithArray:copy];
                                            [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                                        }];
                                    }];
                                }];
}


@end
