//
//  DLDropletViewController.m
//  Dropplet
//
//  Created by Denis Trofimov on 12.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import "DLDropletViewController.h"
#import "DLAction.h"
#import "DLDroplet.h"
#import "UIViewController+Progress.h"
#import "DLAlertController.h"

@interface DLDropletViewController ()

@property(nonatomic, strong)NSArray*actions;

@end

@implementation DLDropletViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = self.droplet.name;
    [self execute:^(Progress progress, Done done) {
        [self.droplet listActionsWithCompletion:^(NSError *error, NSArray *list) {
            done();
            if(error)
                return ALERT(@"error",error.localizedDescription);
            self.actions = list;
            [self.tableView reloadData];
        }];
    }];
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.actions.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"action"];
    DLAction *action = self.actions[indexPath.row];
    cell.textLabel.text = action.localizedType;
    return cell;
}

@end
