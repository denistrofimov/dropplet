//
//  DLDropletTableViewCell.m
//  Dropplet
//
//  Created by Denis Trofimov on 27.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import "DLDropletTableViewCell.h"
#import "DLDroplet.h"
#import "DLRegion.h"
#import "DLSize.h"
#import "DLImage.h"

@interface DLDropletTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *ramLabel;
@property (weak, nonatomic) IBOutlet UILabel *cpuLabel;
@property (weak, nonatomic) IBOutlet UILabel *diskLabel;
@property (weak, nonatomic) IBOutlet UILabel *transferLabel;
@property (weak, nonatomic) IBOutlet UIImageView *regionImageView;
@property (weak, nonatomic) IBOutlet UIImageView *distributionImageView;

@end

@implementation DLDropletTableViewCell

-(void)awakeFromNib{
    [super awakeFromNib];
    self.ramLabel.layer.cornerRadius = 4;
    self.ramLabel.layer.masksToBounds = YES;
    self.cpuLabel.layer.cornerRadius = 4;
    self.cpuLabel.layer.masksToBounds = YES;
    self.diskLabel.layer.cornerRadius = 4;
    self.diskLabel.layer.masksToBounds = YES;
    self.transferLabel.layer.cornerRadius = 4;
    self.transferLabel.layer.masksToBounds = YES;
}

-(void)setDroplet:(DLDroplet *)droplet{
    _droplet = droplet;
    self.nameLabel.text = droplet.name;
    self.addressLabel.text = droplet.ip;
    self.ramLabel.text = [NSString stringWithFormat:@"%@m", droplet.memory];
    self.cpuLabel.text = [NSString stringWithFormat:@"%@xCPU", droplet.vcpus];
    self.diskLabel.text = [NSString stringWithFormat:@"%@GB", droplet.disk];
    self.transferLabel.text = [NSString stringWithFormat:@"%@TB", droplet.size.transfer];
    self.regionImageView.image = droplet.region.flag;
    self.distributionImageView.image = droplet.image.distributionIcon;
}

@end
