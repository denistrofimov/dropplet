//
//  DLActionCollectionViewCell.h
//  Dropplet
//
//  Created by Denis Trofimov on 27.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DLActionCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *textLabel;

@end
