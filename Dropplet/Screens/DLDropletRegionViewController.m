//
//  DLDropletRegionViewController.m
//  Dropplet
//
//  Created by Denis Trofimov on 18.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import "DLDropletRegionViewController.h"
#import "DLDropletImageViewController.h"
#import "UIViewController+Progress.h"
#import "DLRegion.h"
#import "DLDroplet.h"

@interface DLDropletRegionViewController ()

@property (weak, nonatomic) IBOutlet UIBarButtonItem *nextButton;
@property (nonatomic, strong)NSArray *regions;
@property (nonatomic, strong) NSIndexPath *selectedPath;
@end

@implementation DLDropletRegionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self execute:^(Progress progress, Done done) {
        [DLRegion listWithCompletion:^(NSError *error, NSArray *list) {
            done();
            list = [list sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES]]];
            self.regions = list;
            [self.tableView reloadData];
        }];
    }];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.regions.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"region" forIndexPath:indexPath];
    
    DLRegion *region = self.regions[indexPath.row];
    cell.textLabel.text = [NSString stringWithFormat:@"%@", region.name];
    cell.imageView.image = region.flag;
    if(self.droplet.region && [region.uid isEqualToString:self.droplet.region.uid])
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    else
        cell.accessoryType = UITableViewCellAccessoryNone;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(self.selectedPath)
        [tableView cellForRowAtIndexPath:self.selectedPath].accessoryType = UITableViewCellAccessoryNone;
    [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
    self.selectedPath = indexPath;
    self.droplet.region = self.regions[indexPath.row];
    self.nextButton.enabled = YES;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    DLDropletImageViewController* controller = [segue destinationViewController];
    controller.droplet = self.droplet;
}


@end
