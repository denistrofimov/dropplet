//
//  DLCreateDomainViewController.m
//  Dropplet
//
//  Created by Denis Trofimov on 24.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import "DLCreateDomainViewController.h"
#import "DLDroplet.h"
#import "DLDomain.h"
#import "DLAlertController.h"
#import "UIViewController+Progress.h"

@interface DLCreateDomainViewController()

@property(nonatomic, strong)NSArray *droplets;

@property (weak, nonatomic) IBOutlet UITextField *domainTextField;
@property (weak, nonatomic) IBOutlet UITextField *ipTextField;

- (IBAction)cancelAction:(id)sender;
- (IBAction)doneAction:(id)sender;

@end

@implementation DLCreateDomainViewController

-(void)viewDidLoad{
    
    [self execute:^(Progress progress, Done done) {
        [DLDroplet listWithCompletion:^(NSError *error, NSArray *list) {
            done();
            self.droplets = list;
            [self.tableView reloadSections:[[NSIndexSet alloc] initWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
        }];
    }];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.droplets.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"droplet"];

    DLDroplet *droplet = self.droplets[indexPath.row];
    cell.textLabel.text = [NSString stringWithFormat:@"%@", droplet.name];
    cell.detailTextLabel.text = droplet.networks[@"v4"][0][@"ip_address"];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    DLDroplet *droplet = self.droplets[indexPath.row];
    self.ipTextField.text = droplet.networks[@"v4"][0][@"ip_address"];
    
}

- (IBAction)cancelAction:(id)sender {
    [self.navigationController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)doneAction:(id)sender {
    
    if(!self.domainTextField.text.length)
        return ALERT(@"error",@"empty domain field error");
    
    if(!self.ipTextField.text.length)
        return ALERT(@"error",@"empty ip field error");
    
    DLDomain *domain = [[DLDomain alloc] init];
    domain.name = self.domainTextField.text;
    [self execute:^(Progress progress, Done done) {
        [domain createWithParameters:@{
                                       @"ip_address":self.ipTextField.text
                                       }
                          completion:^(NSError *error) {
                              done();
                              if(error)
                                  return ALERT(@"error",error.localizedDescription);
                              [self.navigationController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
                          }];
    }];
    
    
}

@end
