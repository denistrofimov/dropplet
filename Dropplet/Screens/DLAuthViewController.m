//
//  DLAuthViewController.m
//  Dropplet
//
//  Created by Denis Trofimov on 12.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import "DLAuthViewController.h"
#import "DLAPI.h"
#import "MBProgressHUD.h"
#import "DLAlertController.h"

@interface DLAuthViewController () <UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation DLAuthViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (NSHTTPCookie *cookie in [storage cookies]) {
        [storage deleteCookie:cookie];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self.webView loadRequest:[NSURLRequest requestWithURL:API.authURL]];
}

-(NSDictionary*)parseURL:(NSURL*)url{
    NSArray *comp1 = [[url absoluteString] componentsSeparatedByString:@"#"];
    NSString *query = [comp1 lastObject];
    NSArray *queryElements = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    for (NSString *element in queryElements) {
        NSArray *keyVal = [element componentsSeparatedByString:@"="];
        if (keyVal.count > 0) {
            NSString *variableKey = [keyVal objectAtIndex:0];
            id value = [NSNull null];
            if(keyVal.count == 2) value = [keyVal lastObject];
            [dict setObject:value forKey:variableKey];
        }
    }
    return [NSDictionary dictionaryWithDictionary:dict];
}

-(void)webViewDidStartLoad:(UIWebView *)webView{
    NSDictionary *data = [self parseURL:[[webView request] URL]];
    if(data[@"access_token"]){
        [API saveToken:data[@"access_token"] completion:^(NSError *error) {
            if(error)
                ALERT(@"error", error.localizedDescription);
            [self.navigationController popViewControllerAnimated:YES];
        }];
    }else{
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    NSLog(@"%@", error);
}

@end
