//
//  DLPreAuthViewController.m
//  Dropplet
//
//  Created by Denis Trofimov on 12.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import "DLPreAuthViewController.h"
#import "DLAPI.h"

@interface DLPreAuthViewController ()

@end

@implementation DLPreAuthViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.alpha = 0;
}

-(void)viewDidAppear:(BOOL)animated{
    if(API.account)
        [self performSegueWithIdentifier:@"app" sender:nil];
    else{
        [UIView animateWithDuration:0.25 animations:^{
            self.view.alpha = 1;
        }];
    }
}

@end
