//
//  DLMXDomainRecordViewController.m
//  Dropplet
//
//  Created by Denis Trofimov on 24.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import "DLMXDomainRecordViewController.h"
#import "DLDomainRecord.h"
#import "DLAlertController.h"

@interface DLMXDomainRecordViewController()

@property (weak, nonatomic) IBOutlet UITextField *hostTextField;
@property (weak, nonatomic) IBOutlet UITextField *priorityTextField;

@end

@implementation DLMXDomainRecordViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    self.priorityTextField.text = [self.record.priority stringValue];
    self.hostTextField.text = self.record.data;
}

-(void)doneAction:(id)sender{
    if(!self.hostTextField.text.length)
        return ALERT(@"error",@"empty host field error");
    
    if(!self.priorityTextField.text.length)
        return ALERT(@"error",@"empty priority field error");
    
    self.record.data = self.hostTextField.text;
    if(![self.record.data hasSuffix:@"."])
        self.record.data = [NSString stringWithFormat:@"%@.", self.record.data];
    self.record.priority = @([self.priorityTextField.text integerValue]);
    
    [super doneAction:sender];
}

@end
