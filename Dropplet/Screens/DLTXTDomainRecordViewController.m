//
//  DLTXTDomainRecordViewController.m
//  Dropplet
//
//  Created by Denis Trofimov on 24.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import "DLTXTDomainRecordViewController.h"
#import "DLDomainRecord.h"
#import "DLAlertController.h"

@interface DLTXTDomainRecordViewController()

@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *textTextField;

@end

@implementation DLTXTDomainRecordViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    self.nameTextField.text = self.record.name;
    self.textTextField.text = self.record.data;
}

-(void)doneAction:(id)sender{
    if(!self.nameTextField.text.length)
        return ALERT(@"error",@"empty name field error");
    
    if(!self.textTextField.text.length)
        return ALERT(@"error",@"empty text field error");
    
    self.record.data = self.textTextField.text;
    self.record.name = self.nameTextField.text;
    
    [super doneAction:sender];
}

@end
