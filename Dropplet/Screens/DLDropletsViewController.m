//
//  DLDropletsViewController.m
//  Dropplet
//
//  Created by Denis Trofimov on 12.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import "DLDropletsViewController.h"
#import "DLDropletViewController.h"
#import "DLDroplet.h"
#import "DLAction.h"
#import "DLDomain.h"
#import "DLDropletTableViewCell.h"
#import "UIViewController+ECSlidingViewController.h"
#import "DLAlertController.h"
#import "UIViewController+Progress.h"

@interface DLDropletsViewController ()<SWTableViewCellDelegate>

@property(nonatomic, strong)NSArray *droplets;
@property(nonatomic, strong)NSIndexPath *deleteIndexPath;

@end

@implementation DLDropletsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated{
    [self execute:^(Progress progress, Done done) {
        [DLDroplet listWithCompletion:^(NSError *error, NSArray *list) {
            self.droplets = list;
            [self.tableView reloadSections:[[NSIndexSet alloc] initWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
            done();
        }];
    }];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"droplet"]){
        DLDropletViewController * dropletViewController = [segue destinationViewController];
        dropletViewController.droplet = sender;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.droplets.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    DLDropletTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"droplet"];
    DLDroplet *droplet = self.droplets[indexPath.row];
    cell.droplet = droplet;
    cell.delegate = self;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self performSegueWithIdentifier:@"droplet" sender:self.droplets[indexPath.row]];
}

-(BOOL)swipeableTableViewCell:(SWTableViewCell *)cell canSwipeToState:(SWCellState)state{
    return self.slidingViewController.currentTopViewPosition == ECSlidingViewControllerTopViewPositionCentered;
}

- (void)swipeableTableViewCell:(SWTableViewCell *)cell scrollingToState:(SWCellState)state{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    switch(state){
        case kCellStateCenter:
            self.slidingViewController.panGesture.enabled = YES;
            self.deleteIndexPath = nil;
            break;
        case kCellStateLeft:
        case kCellStateRight:{
            self.slidingViewController.panGesture.enabled = NO;
            if(self.deleteIndexPath && self.deleteIndexPath.row != indexPath.row)
                [(DLSwipeToDeleteTableViewCell*)[self.tableView cellForRowAtIndexPath:self.deleteIndexPath] hideUtilityButtonsAnimated:YES];
            self.deleteIndexPath = indexPath;
        }
            break;
    }
}

- (void)swipeableTableViewCell:(DLSwipeToDeleteTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index {
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    [DLAlertController confirmWithTitle:NSLocalizedString(@"Warning", nil)
                                message:[NSString stringWithFormat:NSLocalizedString(@"delete entity warning", nil), NSLocalizedString(@"droplet", nil)]
                                 action:NSLocalizedString(@"Delete",nil)
                                handler:^{
                                    DLDroplet *droplet = self.droplets[indexPath.row];
                                    [self execute:^(Progress progress, Done done) {
                                        [droplet deleteWithCompletion:^(NSError *error) {
                                            done();
                                            if(error)
                                                return ALERT(@"error",error.localizedDescription);
                                            NSMutableArray *copy = self.droplets.mutableCopy;
                                            [copy removeObject:droplet];
                                            self.droplets = [NSArray arrayWithArray:copy];
                                            [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                                        }];
                                    }];
                                }];
}

@end
