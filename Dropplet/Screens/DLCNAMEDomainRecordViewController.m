//
//  DLCNAMEDomainRecordViewController.m
//  Dropplet
//
//  Created by Denis Trofimov on 24.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import "DLCNAMEDomainRecordViewController.h"
#import "DLDomainRecord.h"
#import "DLAlertController.h"

@interface DLCNAMEDomainRecordViewController()

@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *hostTextField;

@end

@implementation DLCNAMEDomainRecordViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    self.nameTextField.text = self.record.name;
    self.hostTextField.text = self.record.data;
}

-(void)doneAction:(id)sender{
    if(!self.nameTextField.text.length)
        return ALERT(@"error",@"empty name field error");
    
    if(!self.hostTextField.text.length)
        return ALERT(@"error",@"empty host field error");
    
    self.record.data = self.hostTextField.text;
    if(![self.record.data hasSuffix:@"."])
        self.record.data = [NSString stringWithFormat:@"%@.", self.record.data];
    self.record.name = self.nameTextField.text;
    
    [super doneAction:sender];
}

@end
