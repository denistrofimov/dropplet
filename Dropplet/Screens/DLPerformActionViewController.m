//
//  DLPerformActionViewController.m
//  Dropplet
//
//  Created by Denis Trofimov on 25.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import "DLPerformActionViewController.h"
#import "DLActionCollectionViewCell.h"
#import "DLAction.h"
#import "DLAlertController.h"

@interface DLPerformActionViewController()

- (IBAction)cancelAction:(id)sender;

@end

@implementation DLPerformActionViewController

- (IBAction)cancelAction:(id)sender {
    [self.navigationController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [DLAction types].count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    DLActionCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"action" forIndexPath:indexPath];
    cell.textLabel.text = NSLocalizedString([DLAction types][indexPath.row], nil);
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(100, 100);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NSString *type = [DLAction types][indexPath.row];
    if([DLAction isSimple:type]){
        [DLAlertController confirmWithTitle:NSLocalizedString(@"Warning", nil)
                                    message:[NSString stringWithFormat:NSLocalizedString(@"perform action warning", nil), NSLocalizedString(type,nil)]
                                     action:NSLocalizedString(@"Perform", nil)
                                    handler:^{
                                    
                                    }];
    }
}

@end
