//
//  DLCreateDropletViewController.m
//  Dropplet
//
//  Created by Denis Trofimov on 18.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import "DLCreateDropletViewController.h"
#import "DLAPI.h"
#import "DLDroplet.h"
#import "DLSSHKey.h"
#import "DLAlertController.h"
#import "UIViewController+Progress.h"

@interface DLCreateDropletViewController ()

@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UISwitch *backupSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *ipv6Switch;
@property (weak, nonatomic) IBOutlet UISwitch *privateNetworkingSwitch;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *doneButton;

-(IBAction)done:(id)sender;

@end

@implementation DLCreateDropletViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

-(IBAction)done:(id)sender{
    
    if(!self.nameTextField.text.length)
        return ALERT(@"error", @"missed droplet name error");
    
    self.droplet.name = self.nameTextField.text;
    
    NSMutableArray *keys = [NSMutableArray array];
    
    for (DLSSHKey*key in self.keys)
        [keys addObject:key.uid];
    
    NSDictionary *info = @{
                           @"backups":@(self.backupSwitch.on),
                           @"ipv6":@(self.ipv6Switch.on),
                           @"private_networking":@(self.privateNetworkingSwitch.on),
                           @"ssh_keys":keys
                           };
    
    
    [self execute:^(Progress progress, Done done) {
        [self.droplet createWithParameters:info completion:^(NSError *error) {
            done();
            if(error)
                return ALERT(@"error", error.localizedDescription);
            
            [self.navigationController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
        }];
    }];
}

@end
