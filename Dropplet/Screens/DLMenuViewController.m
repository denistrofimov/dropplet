//
//  DLMenuViewController.m
//  Dropplet
//
//  Created by Denis Trofimov on 12.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import "DLMenuViewController.h"
#import "UIColor+RGB.h"
#import "UIViewController+ECSlidingViewController.h"

@interface DLMenuViewController ()

@end

@implementation DLMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithRGB:0x2b74a7];
    self.tableView.contentInset = UIEdgeInsetsMake(33, 0, 0, 0);
}

- (IBAction)unwindToMenuViewController:(UIStoryboardSegue *)segue {
    if (self.slidingViewController.currentTopViewPosition == ECSlidingViewControllerTopViewPositionCentered) {
        [self.slidingViewController anchorTopViewToRightAnimated:YES];
    } else {
        [self.slidingViewController resetTopViewAnimated:YES];
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [cell setBackgroundColor:[UIColor clearColor]];
    cell.textLabel.textColor = [UIColor whiteColor];
}

@end
