//
//  ADAddDropletViewController.h
//  Dropplet
//
//  Created by Denis Trofimov on 13.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DLDroplet;

@interface DLDropletSizeViewController : UITableViewController

@property(nonatomic, strong) DLDroplet *droplet;

@end
