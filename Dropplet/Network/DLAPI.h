//
//  DLAPI.h
//  Dropplet
//
//  Created by Denis Trofimov on 13.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import "AFHTTPRequestOperationManager.h"

#define ERROR(_code_, _description_) [NSError errorWithDomain:@"com.digitalocean.error" code:(_code_) userInfo:@{NSLocalizedDescriptionKey:(_description_)}]
#define API DLAPI.manager

@interface DLAPI : AFHTTPRequestOperationManager

@property(nonatomic, strong) NSString* account;
@property(nonatomic, readonly)NSArray*accounts;
@property(nonatomic, readonly)NSURL* authURL;

-(void)deleteAccount:(NSString*)account;

-(void)saveToken:(NSString *)accessToken completion:(void (^)(NSError* error))completion;

-(void)accountWithCompletion:(void (^)(NSError* error, id info))completion;

-(void)getEntitiesWithName:(NSString*)entity completion:(void (^)(NSError* error, NSArray *list))completion;

-(void)getEntitiesWithName:(NSString*)entity parameters:(NSDictionary*)parameters completion:(void (^)(NSError* error, NSArray *list))completion;

-(void)getEntitiesWithName:(NSString*)entity parameters:(NSDictionary*)parameters entry:(NSString*)entry completion:(void (^)(NSError* error, NSArray *list))completion;

-(void)getEntityWithName:(NSString*)entity uid:(id)uid completion:(void (^)(NSError* error, id entity))completion;

-(void)createEntityWithName:(NSString*)entity data:(id)data completion:(void (^)(NSError* error, id entity))completion;

-(void)updateEntityWithName:(NSString*)entity uid:(id)uid data:(id)data completion:(void (^)(NSError* error, id entity))completion;

-(void)deleteEntityWithName:(NSString*)entity uid:(id)uid completion:(void (^)(NSError* error))completion;


@end
