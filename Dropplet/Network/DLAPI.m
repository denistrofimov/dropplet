//
//  DLAPI.m
//  Dropplet
//
//  Created by Denis Trofimov on 13.07.15.
//  Copyright (c) 2015 denistrofimov.com. All rights reserved.
//

#import "DLAPI.h"
#import "DLKeychain.h"

#define API_TOKEN @"1926374ccac3c9e2c0dc2c940564949a6d33f571c1fd5ed41ee9c01786a2a0a9"
#define SERVICE @"digitalocean.com"

@implementation DLAPI

-(instancetype)initWithBaseURL:(NSURL *)url{
    self = [super initWithBaseURL:[NSURL URLWithString:@"https://api.digitalocean.com/v2/"]];
    if(self){
        self.requestSerializer = [AFJSONRequestSerializer serializer];
        NSArray *accounts = [self accounts];
        if(accounts.count)
            self.account = accounts[0][@"acct"];
    }
    return self;
}

-(void)error:(NSError*)error{
    
}

-(void)setAccount:(NSString *)account{
    NSAssert(account != nil, @"Account string can't be nil");
    _account = account;
    NSString *accessToken = [DLKeychain passwordForService:SERVICE account:_account];
    if(accessToken)
        [self.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@", accessToken] forHTTPHeaderField:@"Authorization"];
}

-(NSArray *)accounts{
    return [DLKeychain accountsForService:SERVICE];
}

-(void)deleteAccount:(NSString*)account{
    [DLKeychain deletePasswordForService:SERVICE account:account];
}

-(void)saveToken:(NSString *)accessToken completion:(void (^)(NSError* error))completion{
    NSAssert(accessToken != nil, @"Access token string can't be nil");
    [self.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@", accessToken] forHTTPHeaderField:@"Authorization"];
    [self accountWithCompletion:^(NSError *error, id info) {
        if(error)
            return completion(error);
        _account = info[@"email"];
        [DLKeychain setPassword:accessToken forService:SERVICE account:_account];
        completion(nil);
    }];
}

-(NSURL *)authURL{
    return [NSURL URLWithString:[NSString stringWithFormat:@"https://cloud.digitalocean.com/v1/oauth/authorize?%@",
                                 [self urlParams:@{
                                                   @"response_type": @"token",
                                                   @"client_id":     API_TOKEN,
                                                   @"redirect_uri":  @"http://denistrofimov.com/blank.html",
                                                   @"scope":         @"read%20write"
                                                   }]
                                 ]];
    
}

-(void)accountWithCompletion:(void (^)(NSError* error, id info))completion{
    [self GET:@"account"
   parameters:nil
      success:^(AFHTTPRequestOperation *operation, id responseObject) {
          completion(nil, responseObject[@"account"]);
      }
      failure:^(AFHTTPRequestOperation *operation, NSError *error) {
          error = (operation.responseObject[@"message"]) ? ERROR(error.code, operation.responseObject[@"message"]) : error;
          [self error:error];
          completion(error, nil);
      }];
}

-(NSString*)urlParams:(NSDictionary*)data{
    NSMutableString *string = [NSMutableString string];
    for(NSString*key in data.allKeys){
        if([data.allKeys indexOfObject:key] != 0)
            [string appendString:@"&"];
        [string appendFormat:@"%@=%@", key, data[key]];
    }
    return string;
}

-(void)getEntitiesWithName:(NSString*)entity completion:(void (^)(NSError* error, NSArray *list))success{
    [self getEntitiesWithName:entity parameters:nil completion:success];
}

-(void)getEntitiesWithName:(NSString*)entity parameters:(NSDictionary*)parameters completion:(void (^)(NSError* error, NSArray *list))success{
    [self getEntitiesWithName:entity parameters:parameters entry:entity completion:success];
}

-(void)getEntitiesWithName:(NSString*)entity parameters:(NSDictionary*)parameters entry:(NSString*)entry completion:(void (^)(NSError* error, NSArray *list))success{
    [self GET:entity
   parameters:parameters
      success:^(AFHTTPRequestOperation *operation, id responseObject) {
          success(nil, responseObject[entry] ? responseObject[entry] : responseObject);
      }
      failure:^(AFHTTPRequestOperation *operation, NSError *error) {
          error = (operation.responseObject[@"message"]) ? ERROR(error.code, operation.responseObject[@"message"]) : error;
          [self error:error];
          success(error, nil);
      }];
}

-(void)getEntityWithName:(NSString*)entity uid:(id)uid completion:(void (^)(NSError* error, id entity))success{
    [self GET:[NSString stringWithFormat:@"%@/%@",entity, uid]
   parameters:nil
      success:^(AFHTTPRequestOperation *operation, id responseObject) {
          success(nil, responseObject);
      }
      failure:^(AFHTTPRequestOperation *operation, NSError *error) {
          error = (operation.responseObject[@"message"]) ? ERROR(error.code, operation.responseObject[@"message"]) : error;
          [self error:error];
          success(error, nil);
      }];
}

-(void)createEntityWithName:(NSString*)entity data:(id)data completion:(void (^)(NSError* error, id entity))success{
    [self POST:entity
    parameters:data
       success:^(AFHTTPRequestOperation *operation, id responseObject) {
           success(nil, responseObject);
       }
       failure:^(AFHTTPRequestOperation *operation, NSError *error) {
           error = (operation.responseObject[@"message"]) ? ERROR(error.code, operation.responseObject[@"message"]) : error;
           [self error:error];
           success(error, nil);
       }];
}

-(void)updateEntityWithName:(NSString*)entity uid:(id)uid data:(id)data completion:(void (^)(NSError* error, id entity))success{
    [self PUT:[NSString stringWithFormat:@"%@/%@",entity, uid]
      parameters:data
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             success(nil, responseObject);
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             error = (operation.responseObject[@"message"]) ? ERROR(error.code, operation.responseObject[@"message"]) : error;
             [self error:error];
             success(error, nil);
         }];
}

-(void)deleteEntityWithName:(NSString*)entity uid:(id)uid completion:(void (^)(NSError* error))success{
    [self DELETE:[NSString stringWithFormat:@"%@/%@",entity, uid]
   parameters:nil
      success:^(AFHTTPRequestOperation *operation, id responseObject) {
          success(nil);
      }
      failure:^(AFHTTPRequestOperation *operation, NSError *error) {
          error = (operation.responseObject[@"message"]) ? ERROR(error.code, operation.responseObject[@"message"]) : error;
          [self error:error];
          success(error);
      }];
}

@end
